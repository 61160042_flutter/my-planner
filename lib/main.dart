import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:my_planner_app/widget/calender_widget.dart';
import 'package:my_planner_app/widget/complete_list_widget.dart';
import 'package:my_planner_app/widget/conclusion_widget.dart';
import 'package:my_planner_app/widget/login_widget.dart';
import 'package:my_planner_app/widget/setting_widget.dart';
import 'package:my_planner_app/widget/star_list_widget.dart';
import 'package:my_planner_app/widget/todo_list_widget.dart';
import 'package:my_planner_app/widget/todo_form.dart';
import 'package:my_planner_app/varaible/color_variable.dart';

Future<void> main() async {
  //init firebase
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  late StreamSubscription<User?> _sub;
  static const String _title = 'My Planner';

  @override
  void initState() {
    super.initState();
    _sub = FirebaseAuth.instance.authStateChanges().listen((event) {
      _navigatorKey.currentState!
          .pushReplacementNamed(event != null ? 'home' : 'login');
    });
  }

  @override
  void dispose() {
    _sub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      navigatorKey: _navigatorKey,
      initialRoute:
          FirebaseAuth.instance.currentUser == null ? 'login' : 'home',
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case 'home':
            return MaterialPageRoute(
                settings: settings, builder: (context) => MyPlannerWidget());
          case 'login':
            return MaterialPageRoute(
                settings: settings, builder: (context) => LoginWidget());
          default:
            return MaterialPageRoute(
                settings: settings, builder: (context) => SettingWidget());
        }
      },
      localizationsDelegates: [
        GlobalWidgetsLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [Locale('en', 'US')],
    );
  }
}

class MyPlannerWidget extends StatefulWidget {
  MyPlannerWidget({Key? key}) : super(key: key);

  @override
  _MyPlannerWidgetState createState() => _MyPlannerWidgetState();
}

class _MyPlannerWidgetState extends State<MyPlannerWidget> {
  List menuList = ['TODO LIST', 'CALENDER', 'MY PLANNER'];
  List drawerList = ['TODO LIST', 'COMPLETE', 'STAR'];
  bool checkAddPage = false;
  int _selectedIndex = 0;
  static List<StatefulWidget> _widgetOptions = <StatefulWidget>[
    TodoListWidget(),
    CalendarWidget(),
    ConclusionWidget()
  ];
  bool status = false;

  void _onItemTapped(int index) {
    if (status) {
      _widgetOptions[0] = TodoListWidget();
      menuList[0] = 'TODO LIST';
    }
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          menuList[_selectedIndex],
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        backgroundColor: pink,
      ),
      drawer: (_selectedIndex == 0)
          ? Drawer(
              child: ListView(
                children: [
                  DrawerHeader(
                    child: Text('Menu'),
                    decoration: BoxDecoration(color: pink),
                  ),
                  ListTile(
                    leading: Icon(Icons.list_alt),
                    title: Text('all'),
                    onTap: () {
                      Navigator.pop(context);
                      setState(() {
                        _widgetOptions[0] = TodoListWidget();
                        status = true;
                        menuList[0] = drawerList[0];
                      });
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.check),
                    title: Text('complete'),
                    onTap: () {
                      Navigator.pop(context);
                      setState(() {
                        _widgetOptions[0] = CompleteListWidget();
                        status = true;
                        menuList[0] = drawerList[1];
                      });
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.star),
                    title: Text('star'),
                    onTap: () {
                      Navigator.pop(context);
                      setState(() {
                        _widgetOptions[0] = StarListWidget();
                        status = true;
                        menuList[0] = drawerList[2];
                      });
                    },
                  ),
                ],
              ),
            )
          : null,
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            label: 'todo list',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today),
            label: 'calender',
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.person,
            ),
            label: 'my planner',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.black54,
        onTap: _onItemTapped,
        backgroundColor: darkPink,
        unselectedItemColor: Colors.white,
      ),
      floatingActionButton: new Visibility(
        child: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => TodoForm(
                          todoId: '',
                        )));
          },
          backgroundColor: darkYellow,
        ),
        visible: (_selectedIndex == 0 && checkAddPage == false) ? true : false,
      ),
    );
  }
}
