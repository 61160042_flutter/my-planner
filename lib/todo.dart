class Todo {
  int id;
  String title;
  String detail;
  String date;
  String time;
  bool starStatus;
  bool completeStutus;
  int score;
  String improvement;

  Todo(
      {required this.id,
      required this.title,
      required this.detail,
      required this.date,
      required this.time,
      required this.starStatus,
      required this.completeStutus,
      required this.score,
      required this.improvement});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'detail': detail,
      'date': date,
      'time': time,
      'score': score,
      'improvement': improvement,
      'startStatus' : starStatus,
      'complateStatus': completeStutus
    };
  }

  static List<Todo> toList(List<Map<String, dynamic>> maps) {
    return List.generate(maps.length, (i) {
      return Todo(
          id: maps[i]['id'],
          title: maps[i]['title'],
          detail: maps[i]['detail'],
          date: maps[i]['date'],
          time: maps[i]['time'],
          score: maps[i]['score'],
          improvement: maps[i]['improvement'],
          starStatus: maps[i]['starStatus'],
          completeStutus: maps[i]['completeStatus']);
    });
  }

  @override
  String toString() {
    return 'Todo {$id: $title $detail $date $time $score $improvement complete: $completeStutus}';
  }
}
