import 'todo.dart';

var lastId = 4;
var todoDefault = Todo(
    id: -1,
    date: '2021-11-11',
    detail: '',
    title: '',
    time: '00:00',
    score: 3,
    improvement: '',
    starStatus: false,
    completeStutus: false);
var todoList = [
  Todo(
      id: 1,
      title: 'การบ้านโมบาย',
      detail: 'ส่งใน classroom',
      date: '2021-10-13',
      time: '23:59',
      score: 3,
      improvement: '',
      starStatus: false,
      completeStutus: false),
  Todo(
      id: 2,
      title: 'การบ้านโมบาย2',
      detail: 'ส่งใน classroom',
      date: '2021-10-15',
      time: '23:59',
      score: 3,
      improvement: '',
      starStatus: true,
      completeStutus: false),
  Todo(
      id: 3,
      title: 'การบ้าน Dev',
      detail: 'ส่งใน classroom',
      date: '2021-10-20',
      time: '23:59',
      score: 3,
      improvement: 'work',
      starStatus: false,
      completeStutus: true),
  Todo(
      id: 4,
      title: 'การบ้าน EQ',
      detail: 'ส่งใน classroom',
      date: '2021-10-20',
      time: '23:59',
      score: 3,
      improvement: 'rrrrrrrrrrrrr',
      starStatus: false,
      completeStutus: true)
];

int getNewId() {
  return ++lastId;
}

void getTodo() {
  print(todoList);
}

int countUncomplete() {
  int count = 0;
  for (var i = 0; i < todoList.length; i++) {
    if (!todoList[i].completeStutus) count++;
  }
  return count;
}

int countComplete() {
  int count = todoList.length - countUncomplete();
  return count;
}

List<double> getScore(){
  List<double> scoreList = [0,0,0];
  for (var i = 0; i < todoList.length; i++) {
    if (todoList[i].completeStutus == true){
      if(todoList[i].score == 3){
        scoreList[0]++;
      }
      else if(todoList[i].score == 2){
        scoreList[1]++;
      }else{
        scoreList[2]++;
      }
    }
  }
  return scoreList;
}
Future<List<Todo>>? getUncomplete() {
  var list = [todoDefault];
  list.clear();
  for (var i = 0; i < todoList.length; i++) {
    if (todoList[i].completeStutus == false){
      list.add(todoList[i]);
    }
  }
  if(list.isEmpty){
    return null;
  }
  return Future.delayed(Duration(seconds: 1), () => list);
}

Future<List<Todo>>? getComplete() {
  var list = [todoDefault];
  list.clear();
  for (var i = 0; i < todoList.length; i++) {
    if (todoList[i].completeStutus == true){
      list.add(todoList[i]);
    }
  }
  if(list.isEmpty){
    return null;
  }
  return Future.delayed(Duration(seconds: 1), () => list);
}


Future<List<Todo>>? getTodoList() {
  if(todoList.isEmpty){
    return null;
  }
  return Future.delayed(Duration(seconds: 1), () => todoList);
}

Future<void> addNewTodo(Todo todo) {
  return Future.delayed(Duration(seconds: 1), () {
    print('id $lastId');
    todoList.add(Todo(
        id: getNewId(),
        title: todo.title,
        detail: todo.detail,
        date: todo.date,
        time: todo.time,
        score: todo.score,
        improvement: todo.improvement,
        starStatus: todo.starStatus,
        completeStutus: todo.completeStutus));
  });
}

Future<void> saveTodo(Todo todo) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = todoList.indexWhere((list) => list.id == todo.id);
    todoList[index] = todo;
  });
}

Future<void> saveScore(Todo todo) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = todoList.indexWhere((list) => list.id == todo.id);
    todoList[index] = todo;
    todoList[index].completeStutus = true;
  });
}

Future<void> delTodo(Todo todo) {
  return Future.delayed(Duration(seconds: 1), () {
    var index = todoList.indexWhere((list) => list.id == todo.id);
    todoList.removeAt(index);
  });
}
