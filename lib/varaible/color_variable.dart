import 'package:flutter/material.dart';

Color darkYellow = Color(0xFFFDDD94);
Color lightYellow = Color(0xFFFCEDC0);
Color pink = Color(0xFFF5CEC7);
Color darkPink = Color(0xFFE79796);
Color lightPink = Color(0xFFF3E9E3);
Color green = Color(0xFFD5EEBB);
Color red = Color(0xFFF9877C);
Color darkGreen = Color(0xFFC1E699);
Color lightGreen = Color(0xFFE6F5D6);