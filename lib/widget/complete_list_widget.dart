import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:my_planner_app/varaible/color_variable.dart';
import 'package:my_planner_app/widget/todo_detail_widget.dart';

class CompleteListWidget extends StatefulWidget {
  CompleteListWidget({Key? key}) : super(key: key);

  @override
  _CompleteListWidgetState createState() => _CompleteListWidgetState();
}

class _CompleteListWidgetState extends State<CompleteListWidget> {
  String _email = '';
  late final Stream<QuerySnapshot> _todoStream;
  late CollectionReference list;
  var status = 'false';

  @override
  void initState() {
    super.initState();
    setState(() {
      _email = FirebaseAuth.instance.currentUser!.email!;
      print('email $_email');
      _todoStream = FirebaseFirestore.instance
          .collection('todo')
          .where('email', isEqualTo: _email)
          .where('completeStatus', isEqualTo: 'true')
          .snapshots();
      list = FirebaseFirestore.instance.collection('todo');
    });
    print('start');
  }

  Future<void> delTodo(todoId) {
    return list
        .doc(todoId)
        .delete()
        .then((value) => print('delete completed'))
        .catchError((error) => 'Failed to delete todo: $error');
  }

  Future<void> comeback(todoId) {
    return list
        .doc(todoId)
        .update({'completeStatus': 'false', 'timeStamp': 'null'})
        .then((value) => print('comeback toto'))
        .catchError((error) => print('failed to comeback: $error'));
  }
  
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _todoStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Error');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading');
          }
          return ListView(
            children: snapshot.data!.docs.map((DocumentSnapshot document) {
              Map<String?, dynamic> data =
                  document.data()! as Map<String?, dynamic>;
              return Container(
                padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                child: ListTile(
                  title: Text(data['title']),
                  subtitle: Text(
                      '${data['detail']} (${data['date']} ${data['time']})'),
                  trailing: IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                                title: Text('Are you sure to delete this todo'),
                                content: Text(
                                    '${data['title']} => ${data['date']} ${data['time']}'),
                                actions: [
                                  TextButton(
                                      onPressed: () async {
                                        await delTodo(document.id);
                                        Navigator.pop(context, 'Yes');
                                      },
                                      child: const Text('Yes')),
                                  TextButton(
                                      onPressed: () =>
                                          Navigator.pop(context, 'No'),
                                      child: const Text('No')),
                                ],
                              ));
                    },
                  ),
                  leading: IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                                title:
                                    Text('Are you sure to restore this todo'),
                                content: Text(
                                    '${data['title']} => ${data['date']} ${data['time']}'),
                                actions: [
                                  TextButton(
                                      onPressed: () async {
                                        await comeback(document.id);
                                        Navigator.pop(context, 'Yes');
                                      },
                                      child: const Text('Yes')),
                                  TextButton(
                                      onPressed: () =>
                                          Navigator.pop(context, 'No'),
                                      child: const Text('No')),
                                ],
                              ));
                    },
                  ),
                  tileColor: lightGreen,
                ),
              );
            }).toList(),
          );
        });
  }
}
