import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:my_planner_app/todo.dart';
import 'package:my_planner_app/varaible/color_variable.dart';
import 'package:my_planner_app/widget/setting_widget.dart';
import 'package:pie_chart/pie_chart.dart';

class ConclusionWidget extends StatefulWidget {
  ConclusionWidget({Key? key}) : super(key: key);

  @override
  _ConclusionWidgetState createState() => _ConclusionWidgetState();
}

class _ConclusionWidgetState extends State<ConclusionWidget> {
  int _countUn = 0;
  int _countCom = 0;
  double score3 = 0;
  double score2 = 0;
  double score1 = 0;
  String timeStamp = '0000-00-00';
  String improvement = '';

  List<Color> _colorList = [green, darkYellow, red];

  late Future<List<Todo>>? todo;
  String _imgUrl = '';
  String _name = '';
  String _email = '';
  late final Stream<QuerySnapshot> _todoStream;
  late CollectionReference list;
  Map<String, double> map() {
    Map<String, double> _dataMap = {
      '3 = completed on time': score3,
      '2 = almost completed on time': score2,
      '1 = uncompleted on time': score1
    };
    return _dataMap;
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      _countCom = 0;
      _countUn = 0;
      _name = FirebaseAuth.instance.currentUser!.displayName!;
      _imgUrl = FirebaseAuth.instance.currentUser!.photoURL!;
      _email = FirebaseAuth.instance.currentUser!.email!;
      _todoStream = FirebaseFirestore.instance
          .collection('todo')
          .where('email', isEqualTo: _email)
          .snapshots();
    });
  }

  Widget pieChart() {
    return PieChart(
      dataMap: map(),
      chartLegendSpacing: 32,
      colorList: _colorList,
      chartValuesOptions: ChartValuesOptions(
        showChartValuesInPercentage: true,
      ),
      legendOptions: LegendOptions(
        showLegendsInRow: false,
        legendTextStyle: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      emptyColor: lightPink,
    );
  }

  Widget showCountTodo(String title, int count, Color color) {
    return Expanded(
      child: Container(
        padding: EdgeInsets.all(10.0),
        margin: EdgeInsets.all(2.0),
        color: color,
        child: Column(
          children: [
            Container(
              child: Text(
                '$count',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 5),
              child: Text('$title'),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _todoStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Error');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading');
          }
          snapshot.data!.docs.map((DocumentSnapshot document) {
            print('snap');
            Map<String, dynamic> data =
                document.data()! as Map<String, dynamic>;
            if (data['completeStatus'] == 'false') {
              _countUn++;
            } else {
              _countCom++;
              if (data['score'] == 3) {
                score3++;
              } else if (data['score'] == 2) {
                score2++;
              } else {
                score1++;
              }

              var time = timeStamp.compareTo(data['timeStamp']);
              if (time == -1) {
                timeStamp = data['timeStamp'];
                improvement = data['improvement'];
              }
            }
            print('com> $_countCom un> $_countUn');
          }).toList();
          print(timeStamp);
          return Container(
              padding: EdgeInsets.all(10.0),
              child: Column(children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Container(
                            margin: EdgeInsets.all(5.0),
                            width: 50,
                            height: 50,
                            decoration: BoxDecoration(
                              border: Border.all(width: 2, color: pink),
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: NetworkImage(_imgUrl),
                                  fit: BoxFit.fill),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(10.0),
                            child: Text(
                              '$_name',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 5.0),
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(primary: lightYellow),
                          onPressed: () async {
                            await FirebaseAuth.instance.signOut();
                          },
                          child: Container(
                            child: Icon(
                              Icons.logout_sharp,
                              color: Colors.black,
                            ),
                          )),
                    ),
                  ],
                ),
                Row(
                  children: [
                    showCountTodo('completed', _countCom, green),
                    showCountTodo('uncompleted', _countUn, red)
                  ],
                ),
                Container(
                  color: pink,
                  margin: EdgeInsets.all(2.0),
                  padding: EdgeInsets.all(5.0),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.all(5.0),
                            child: Icon(Icons.access_time_sharp),
                          ),
                          Container(
                            margin: EdgeInsets.all(5.0),
                            child: Text(
                              'Punctuality Score',
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      pieChart()
                    ],
                  ),
                ),
                Container(
                    color: pink,
                    margin: EdgeInsets.all(2.0),
                    padding: EdgeInsets.all(5.0),
                    child: Container(
                      padding: EdgeInsets.all(5.0),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Icons.report_problem_sharp,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Text(
                                'Things to improve (recently)',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [(improvement == '') ?Text(
                                  'You have not complete todo list ',
                                  style: TextStyle(fontSize: 14, color: Colors.black26),
                                ):
                                Text(
                                  '"$improvement"',
                                  style: TextStyle(fontSize: 20),
                                ),
                              ])
                        ],
                      ),
                    ))
              ]));
        });
  }
}
