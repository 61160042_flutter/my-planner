import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_planner_app/main.dart';
import 'package:my_planner_app/todo_service.dart';
import 'package:my_planner_app/varaible/color_variable.dart';

import '../todo.dart';

class ScoreWidget extends StatefulWidget {
  String todoId;
  ScoreWidget({Key? key, required this.todoId}) : super(key: key);

  @override
  _ScoreWidgetState createState() => _ScoreWidgetState(this.todoId);
}

class _ScoreWidgetState extends State<ScoreWidget> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  CollectionReference list = FirebaseFirestore.instance.collection('todo');
  String todoId;
  _ScoreWidgetState(this.todoId);

  int scoreValue = 3;
  String improvement = '';
  String email = '';

  Future<void> saveScore() {
    return list
        .doc(this.todoId)
        .update({
          'completeStatus': 'true',
          'score': this.scoreValue,
          'improvement': this.improvement,
          'timeStamp': DateTime.now().toString()
        })
        .then((value) => print('update completed'))
        .catchError((error) => print('failed to update: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'SCORE',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        backgroundColor: pink,
      ),
      body: Container(
          padding: EdgeInsets.all(15.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Container(
                  child: Text('Score:'),
                  alignment: Alignment.centerLeft,
                ),
                Row(
                  children: [
                    Expanded(
                      child: DropdownButton(
                        items: [
                          DropdownMenuItem(
                            child: Text('3 = completed on time'),
                            value: 3,
                          ),
                          DropdownMenuItem(
                            child: Text('2 = almost completed on time'),
                            value: 2,
                          ),
                          DropdownMenuItem(
                            child: Text('1 = uncompleted on time'),
                            value: 1,
                          ),
                        ],
                        value: scoreValue,
                        onChanged: (int? newScore) {
                          setState(() {
                            scoreValue = newScore!;
                          });
                        },
                      ),
                    )
                  ],
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'things to improve:'),
                  onChanged: (String? value) {
                    setState(() {
                      improvement = value!;
                    });
                  },
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter improvement';
                    }
                    if (value.length > 50) {
                      return 'Please enter title (${value.length}/50)';
                    }
                    return null;
                  },
                ),
              ],
            ),
          )),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.send),
        onPressed: () async {
          if (_formKey.currentState!.validate()) {
            await saveScore();
            Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                  builder: (BuildContext context) => MyPlannerWidget()),
              (Route<dynamic> route) => false,
            );
          }
        },
        backgroundColor: darkYellow,
      ),
    );
  }
}
