import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:my_planner_app/varaible/color_variable.dart';


class SettingWidget extends StatefulWidget {
  SettingWidget({Key? key}) : super(key: key);

  @override
  _SettingWidgetState createState() => _SettingWidgetState();
}

class _SettingWidgetState extends State<SettingWidget> {
  String _email = '';
  late CollectionReference list;
  
  @override
  @override
  void initState() {
    super.initState();
    setState(() {
       _email = FirebaseAuth.instance.currentUser!.email!;
      print('email $_email');
      list = FirebaseFirestore.instance.collection('todo');
    });
  }

  // Future<void> delAllData(email) {
  //   return list
  //       .where('email',isEqualTo: email)
  //       .delete()
  //       .then((value) => print('delete completed'))
  //       .catchError((error) => 'Failed to delete todo: $error');
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'SETTING',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        backgroundColor: pink,
      ),
      body: Container(
        padding: EdgeInsets.all(30.0),
        child: Column(
          children: [
            ElevatedButton(
                style: ElevatedButton.styleFrom(primary: green),
                onPressed: () {
                  FirebaseAuth.instance.signOut();
                },
                child: Container(
                  padding: EdgeInsets.all(5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Icon(
                          Icons.logout_sharp,
                          color: Colors.black,
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        child: Text(
                          'Log out with Google',
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                    ],
                  ),
                )),
            SizedBox(
              height: 20,
            ),
            Divider(
              color: pink,
              height: 10,
              thickness: 3,
              indent: 40,
              endIndent: 40,
            ),
            SizedBox(
              height: 20,
            ),
            ElevatedButton(
                style: ElevatedButton.styleFrom(primary: red),
                onPressed: () {},
                child: Container(
                  padding: EdgeInsets.all(5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        child: Icon(
                          Icons.delete,
                          color: Colors.black,
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        child: Text(
                          'Clean all data',
                          style: TextStyle(color: Colors.black),
                        ),
                      ),
                    ],
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
