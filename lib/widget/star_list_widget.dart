import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:my_planner_app/varaible/color_variable.dart';
import 'package:my_planner_app/widget/todo_detail_widget.dart';

class StarListWidget extends StatefulWidget {
  StarListWidget({Key? key}) : super(key: key);

  @override
  _StarListWidgetState createState() => _StarListWidgetState();
}

class _StarListWidgetState extends State<StarListWidget> {
  String _email = '';
  late final Stream<QuerySnapshot> _todoStream;
  late CollectionReference list;
  var status = 'false';

  @override
  void initState() {
    super.initState();
    setState(() {
      _email = FirebaseAuth.instance.currentUser!.email!;
      print('email $_email');
      _todoStream = FirebaseFirestore.instance
          .collection('todo')
          .where('email', isEqualTo: _email)
          .where('completeStatus', isEqualTo: 'false')
          .where('starStatus', isEqualTo: 'true')
          .snapshots();
      list = FirebaseFirestore.instance.collection('todo');
    });
    print('start');
  }

  Future<void> updateStarStatus(String todoId, String status) {
    print('id: $todoId $status');
    if (status == 'true') {
      return list
          .doc(todoId)
          .update({
            'starStatus': 'false',
          })
          .then((value) => print('update completed'))
          .catchError((error) => print('failed to update: $error'));
    }
    return list
        .doc(todoId)
        .update({
          'starStatus': 'true',
        })
        .then((value) => print('update completed'))
        .catchError((error) => print('failed to update: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _todoStream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Error');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text('Loading');
          }
          return ListView(
            children: snapshot.data!.docs.map((DocumentSnapshot document) {
              Map<String?, dynamic> data =
                  document.data()! as Map<String?, dynamic>;
              if (data['starStatus'] == 'true') {
                status = 'true';
              } else {
                status = 'false';
              }
              return Container(
                padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                child: ListTile(
                  title: Text(data['title']),
                  subtitle: Text('${data['date']} ${data['time']}'),
                  trailing: IconButton(
                    icon: Icon(data['starStatus'] == 'true'
                        ? Icons.star
                        : Icons.star_border_outlined),
                    color: (data['starStatus'] == 'true'
                        ? darkYellow
                        : Colors.grey),
                    onPressed: () async {
                      print('$document.id, $status');
                      await updateStarStatus(document.id, status);
                    },
                  ),
                  onTap: () async {
                    await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => TodoDetailWidget(
                                  todoId: document.id,
                                )));
                  },
                  tileColor: lightYellow,
                ),
              );
            }).toList(),
          );
        });
  }
}
