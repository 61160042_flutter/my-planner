import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_planner_app/varaible/color_variable.dart';
import 'package:my_planner_app/widget/score_widget.dart';
import 'package:my_planner_app/widget/todo_form.dart';

import '../main.dart';

class TodoDetailWidget extends StatefulWidget {
  String todoId;
  TodoDetailWidget({Key? key, required this.todoId}) : super(key: key);

  @override
  _TodoDetailWidgetState createState() => _TodoDetailWidgetState(this.todoId);
}

class _TodoDetailWidgetState extends State<TodoDetailWidget> {
  String todoId;
  _TodoDetailWidgetState(this.todoId);
  String title = "";
  TextEditingController _titleController = new TextEditingController();

  late final Stream<QuerySnapshot> _todoStream;
  late CollectionReference list;

  @override
  void initState() {
    super.initState();
    setState(() {
      _todoStream = FirebaseFirestore.instance.collection('todo').snapshots();
      list = FirebaseFirestore.instance.collection('todo');
    });
  }

  Future<void> delTodo() {
    return list
        .doc(todoId)
        .delete()
        .then((value) => print('delete completed'))
        .catchError((error) => 'Failed to delete todo: $error');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'TODO DESCRIPTION',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        backgroundColor: pink,
      ),
      body: StreamBuilder(
          stream: _todoStream,
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              return Text('Error');
            }
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Text('Loading');
            }
            return ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String?, dynamic> data =
                    document.data()! as Map<String?, dynamic>;
                if (document.id == todoId) {
                  return Container(
                    child: Column(
                      children: [
                        Container(
                            padding: EdgeInsets.all(15.0),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.all(5.0),
                                      child: ElevatedButton(
                                          onPressed: () {
                                            showDialog(
                                                context: context,
                                                builder:
                                                    (BuildContext context) =>
                                                        AlertDialog(
                                                          title: Text(
                                                              'Are you sure to delete this todo'),
                                                          content: Text(
                                                              '${data['title']} => ${data['date']} ${data['time']}'),
                                                          actions: [
                                                            TextButton(
                                                                onPressed:
                                                                    () async {
                                                                  await delTodo();
                                                                  Navigator.of(
                                                                          context)
                                                                      .pushAndRemoveUntil(
                                                                    MaterialPageRoute(
                                                                        builder:
                                                                            (BuildContext context) =>
                                                                                MyPlannerWidget()),
                                                                    (Route<dynamic>
                                                                            route) =>
                                                                        false,
                                                                  );
                                                                },
                                                                child:
                                                                    const Text(
                                                                        'Yes')),
                                                            TextButton(
                                                                onPressed: () =>
                                                                    Navigator.pop(
                                                                        context,
                                                                        'No'),
                                                                child:
                                                                    const Text(
                                                                        'No')),
                                                          ],
                                                        ));
                                          },
                                          child: Container(
                                            padding: EdgeInsets.all(10.0),
                                            child: Text(
                                              'Delete',
                                              style: TextStyle(
                                                color: Colors.black,
                                              ),
                                            ),
                                          ),
                                          style: ElevatedButton.styleFrom(
                                              primary: red)),
                                    ),
                                    Container(
                                      margin: EdgeInsets.all(5.0),
                                      child: ElevatedButton(
                                          onPressed: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        ScoreWidget(
                                                          todoId: todoId,
                                                        )));
                                          },
                                          child: Container(
                                            padding: EdgeInsets.all(10.0),
                                            child: Text(
                                              'Complete',
                                              style: TextStyle(
                                                color: Colors.black,
                                              ),
                                            ),
                                          ),
                                          style: ElevatedButton.styleFrom(
                                              primary: darkGreen)),
                                    ),
                                  ],
                                ),
                                Container(
                                  padding: EdgeInsets.all(10.0),
                                  child: Text(
                                    data['title'],
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.all(5.0),
                                  child: Text('> ${data['detail']}'),
                                ),
                                Container(
                                  padding: EdgeInsets.all(5.0),
                                  child: Text(
                                      'Date: ${data['date']} and Time: ${data['time']}'),
                                )
                              ],
                            )),
                      ],
                    ),
                  );
                }
                return Container();
              }).toList(),
            );
          }),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.edit),
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => TodoForm(todoId: todoId)));
        },
        backgroundColor: darkYellow,
      ),
    );
  }
}
