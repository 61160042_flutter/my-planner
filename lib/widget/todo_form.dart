import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:my_planner_app/todo_service.dart';
import 'package:my_planner_app/varaible/color_variable.dart';
import 'package:my_planner_app/widget/todo_detail_widget.dart';

import '../main.dart';
import '../todo.dart';

class TodoForm extends StatefulWidget {
  String todoId;
  TodoForm({Key? key, required this.todoId}) : super(key: key);

  @override
  _TodoFormState createState() => _TodoFormState(this.todoId);
}

class _TodoFormState extends State<TodoForm> {
  String todoId;
  _TodoFormState(this.todoId);
  CollectionReference list = FirebaseFirestore.instance.collection('todo');
  TextEditingController _titleController = new TextEditingController();
  TextEditingController _detailController = new TextEditingController();
  TextEditingController _dateController = new TextEditingController();
  TextEditingController _timeController = new TextEditingController();
  String title = '';
  String detail = '';
  String date = '';
  String time = '';
  String _email = '';

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final dateFormat = DateFormat("yyyy-MM-dd");
  final timeFormat = DateFormat("HH:mm");

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      _email = FirebaseAuth.instance.currentUser!.email!;
      if (this.todoId.isNotEmpty) {
        list.doc(this.todoId).get().then((snapshot) {
          var data = snapshot.data() as Map<String, dynamic>;
          title = data['title'];
          detail = data['detail'];
          date = data['date'];
          time = data['time'];
          _titleController.text = title;
          _detailController.text = detail;
          _dateController.text = date;
          _timeController.text = time;
          print('title $title detail $detail date $date time $time');
        });
      }
    });
  }

  Future<void> addTodo() {
    return list
        .add({
          'title': this.title,
          'detail': this.detail,
          'date': this.date,
          'time': this.time,
          'starStatus': 'false',
          'completeStatus': 'false',
          'score': 3,
          'improvement': 'null',
          'email': this._email,
          'timeStamp': 'null'
        })
        .then((value) => print('add completed'))
        .catchError((error) => print('failed to add: $error'));
  }

  Future<void> updateTodo() {
    return list
        .doc(this.todoId)
        .update({
          'title': this.title,
          'detail': this.detail,
          'date': this.date,
          'time': this.time,
        })
        .then((value) => print('update completed'))
        .catchError((error) => print('failed to update: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'TODO FORM',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        backgroundColor: pink,
      ),
      body: Container(
        padding: EdgeInsets.all(15.0),
        child: Form(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  // initialValue: todo.title,
                  controller: _titleController,
                  decoration: InputDecoration(labelText: 'Titile:'),
                  onChanged: (String? value) {
                    setState(() {
                      title = value!;
                    });
                  },
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter title';
                    }
                    return null;
                  },
                ),
                Container(
                  padding: EdgeInsets.all(15.0),
                ),
                DateTimePicker(
                  type: DateTimePickerType.date,
                  dateMask: 'yyyy-MM-dd',
                  // initialValue: date,
                  controller: _dateController,
                  firstDate: DateTime(2000),
                  lastDate: DateTime(2100),
                  dateLabelText: 'Date:',
                  locale: Locale('en', 'US'),
                  onChanged: (String? value) => setState(() => date = value!),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please select date';
                    }
                    if (value.length > 50) {
                      return 'Please enter title (${value.length}/50)';
                    }
                    return null;
                  },
                ),
                Container(
                  padding: EdgeInsets.all(15.0),
                ),
                DateTimePicker(
                  type: DateTimePickerType.time,
                  controller: _timeController,
                  timePickerEntryModeInput: true,
                  // initialValue: time, //_initialValue,
                  timeLabelText: "Time:",
                  use24HourFormat: true,
                  locale: Locale('en', 'US'),
                  onChanged: (value) => setState(() => time = value),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please select time';
                    }
                    return null;
                  },
                ),
                Container(
                  padding: EdgeInsets.all(15.0),
                ),
                TextFormField(
                  // initialValue: todo.detail,
                  controller: _detailController,
                  decoration: InputDecoration(labelText: 'Detail:'),
                  onChanged: (String? value) {
                    setState(() {
                      detail = value!;
                    });
                  },
                  validator: (String? value) {
                    if (value!.isEmpty || value.length == 0) {
                      return 'Please enter detail';
                    }
                    if (value.length > 50) {
                      return 'Please enter datail (${value.length}/50)';
                    }
                    return null;
                  },
                ),
                Container(
                  padding: EdgeInsets.all(15.0),
                ),
                Container(
                  padding: EdgeInsets.all(5.0),
                  child: ElevatedButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        if (todoId.isEmpty) {
                          await addTodo();
                          Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    MyPlannerWidget()),
                            (Route<dynamic> route) => false,
                          );
                        } else {
                          await updateTodo();
                          Navigator.pop(context);
                        }
                      }
                    },
                    child: Text(
                      'Save',
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(primary: darkYellow),
                  ),
                  alignment: Alignment.centerRight,
                ),
              ],
            )),
      ),
    );
  }
}
